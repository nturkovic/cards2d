﻿using UnityEngine;
using System.Collections;

public class CardBehavior : MonoBehaviour {

	private static ILogger logger = Debug.logger;
	private static string kTAG = "CARDS";

	private Sprite backSprite;
	public Sprite frontSprite;

	public Quaternion to;
	public float speed = 2F;
	public bool isRotating = false;
	public bool isDragging = false;
	public bool flipHalf = false;
	public bool back = true;
	private float distance;

	void Awake() {
		Application.targetFrameRate = -1;
	}

	// Use this for initialization
	void Start () {
		backSprite = gameObject.GetComponent<SpriteRenderer> ().sprite;
	}
	
	// Update is called once per frame
	void Update () {
		if (isRotating) {
			logger.Log(kTAG, "from: " + transform.rotation + " to: " + to + " diff: " + Quaternion.Angle(transform.rotation, to));
			transform.rotation = Quaternion.Lerp (transform.rotation, to, Time.deltaTime * speed);
		}
		float diff = Quaternion.Angle (transform.rotation, to);
		if(diff == 0F) {
			isRotating = false;	
		}
		if(isRotating && diff <= 90F) {
			logger.Log(kTAG, "changing sprite: " + diff);
			gameObject.GetComponent<SpriteRenderer> ().sprite = back ? frontSprite : backSprite;
		}
		if (isDragging) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			Vector3 rayPoint = ray.GetPoint(distance);
			rayPoint.z = transform.position.z;
			transform.position = rayPoint;
		}
	}

	void OnMouseUpAsButton() {
		logger.Log (kTAG, "clicked");
		flipCard ();
	}

	void OnMouseUp () {
		if (isDragging) {
			isDragging = false;
//		} else {
//			logger.Log (kTAG, "Clicked");
//			flipCard ();
		}
	}

	void OnMouseDrag () {
		if (!isRotating) {
			logger.Log (kTAG, "dragging");
			isDragging = true;
			distance = Vector3.Distance(transform.position, Camera.main.transform.position);
		}
	}

	private void flipCard() {
		isRotating = true;
		to = Quaternion.Euler(0, back ? 180 : 0, 0);
		back = !back;
	}
}
